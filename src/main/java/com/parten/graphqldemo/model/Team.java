package com.parten.graphqldemo.model;

public enum Team {
    Chelsea,
    ManCity,
    ManUnited,
    Liverpool,
    Arsenal,
    Brentford
}
