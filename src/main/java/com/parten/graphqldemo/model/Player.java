package com.parten.graphqldemo.model;

public record Player(Integer id, String name, Team team) {
}
