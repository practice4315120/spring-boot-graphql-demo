package com.parten.graphqldemo.service;

import com.parten.graphqldemo.model.Player;
import com.parten.graphqldemo.model.Team;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class PlayerService {
    private List<Player> players = new ArrayList<>();
    AtomicInteger id = new AtomicInteger(0);
    public List<Player> findAll(){
        return players;
    }
    public Optional<Player> findById(Integer id){
        return players.stream().filter(player -> player.id() == id).findFirst();
    }
    public Player create(String name, Team team){
        Player player = new Player(id.incrementAndGet(), name, team);
        players.add(player);
        return player;
    }
    public Player delete(Integer id){
        Player player = players.stream().filter(c -> c.id() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("player not found"));
        players.remove(player);
        return player;
    }
    public Player update(Integer id, String name, Team team){
        Player updatedPlayer = new Player(id, name, team);
        Optional<Player> optional = players.stream()
                .filter(c -> c.id() == id)
                .findFirst();
        if (optional.isPresent()){
            Player player = optional.get();
            int index = players.indexOf(player);
            players.set(index, updatedPlayer);
        }else {
            throw new IllegalArgumentException("Invalid player");
        }
        return updatedPlayer;
    }

    @PostConstruct
    public void init(){
        players.add(new Player(id.incrementAndGet(), "Jackson", Team.Chelsea));
        players.add(new Player(id.incrementAndGet(), "Madueke", Team.Chelsea));
        players.add(new Player(id.incrementAndGet(), "Sterling", Team.Chelsea));
        players.add(new Player(id.incrementAndGet(), "Palmer", Team.Chelsea));
        players.add(new Player(id.incrementAndGet(), "Enzo", Team.Chelsea));
        players.add(new Player(id.incrementAndGet(), "Nkunku", Team.Chelsea));
        players.add(new Player(id.incrementAndGet(), "Foden", Team.ManCity));
        players.add(new Player(id.incrementAndGet(), "KDB", Team.ManCity));
        players.add(new Player(id.incrementAndGet(), "Haaland", Team.ManCity));
        players.add(new Player(id.incrementAndGet(), "Maino", Team.ManUnited));
        players.add(new Player(id.incrementAndGet(), "Salah", Team.Liverpool));
        players.add(new Player(id.incrementAndGet(), "Robertson", Team.Liverpool));
        players.add(new Player(id.incrementAndGet(), "Mbeumo", Team.Brentford));
    }
}
